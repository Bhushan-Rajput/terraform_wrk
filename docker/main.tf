terraform {
    required_providers {
      docker = {
        source = "kreuzwerker/docker"
        version = "3.0.2"
      }
    }
}

provider "docker" {
  
}

resource "docker_image" "ubuntu_tf" {
    name = "ubuntu:latest"
  
}

resource "docker_container" "ubuntu_tf_container" {
    image = docker_image.ubuntu_tf.image_id
    name = "ubuntu_tf_container"
    ports {
	internal = 80
	external = 8000
}
    command = ["sleep", "500"]
   
     
}
